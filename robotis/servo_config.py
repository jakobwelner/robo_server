import math

# home position in encoder ticks for the servo.

servo_param = {
    2: {                        #
        'home_encoder': 512,
        'max_ang': math.radians( 104.0 ),
        'min_ang': math.radians( -104.0 ),
        "controller": "ctrl_02.rotateY", # name of Maya attribute controlling it
        'feedback': 'feed_02.rotateY'
       },
    3: {                        #
        'home_encoder': 512,
        'max_ang': math.radians( 104.0 ),
        'min_ang': math.radians( -104.0 ),
        "controller": "ctrl_03.rotateY", # name of Maya attribute controlling it
        'feedback': 'feed_03.rotateY'
       },
    4: {                        #
        'home_encoder': 512,
        'max_ang': math.radians( 104.0 ),
        'min_ang': math.radians( -104.0 ),
        "controller": "ctrl_04.rotateY", # name of Maya attribute controlling it
        'feedback': "feed_04.rotateY"
       },
    5: {                        #
        'home_encoder': 512,
        'max_ang': math.radians( 104.0 ),
        'min_ang': math.radians( -104.0 ),
        "controller": "ctrl_05.rotateY", # name of Maya attribute controlling it
        'feedback': "feed_05.rotateY"
       },
}