import math
from robotis.lib_robotis import *
from dataset import *
import os

servoDict = {}
BROADCAST = 0
dyn = False

def attach_usb():
      global dyn
      try:
          if os.name == "posix":
                USB_PORT = "/dev/ttyUSB0"
          else:
                USB_PORT = "COM12"
                
          dyn = USB2Dynamixel_Device(USB_PORT)
      except:
          print "couldn't connect to USB. Maybe it's occupied by something else? "
          raise



def release_usb():
    global dyn
    dyn.servo_dev.close()




def broadcast_servos(servodata):
      global BROADCAST
      
      if not BROADCAST:
            BROADCAST = Robotis_Broadcast(dyn)

      BROADCAST.sync_write(servodata)



def disable_torque(ids):
      for id in ids:
            servo = Robotis_Servo(dyn, id)
            servo.disable_torque()
            
def enable_torque(ids):
      for id in ids:
            servo = Robotis_Servo(dyn, id)
            servo.enable_torque()
            


def set_servos(servodata):
    global servoDict
    #print "dataObj received at servo_control: ", dataObj
    #print "dataObj type: ", type(dataObj)

    for id in servodata.get_ids():
        try:
            if not id in servoDict:
               servoDict[id] = Robotis_Servo(dyn, id)

            servo = servoDict[id]

            if servodata.get_type() == "angle":

                  ang = servodata.get_ang(id)
                  vel = servodata.get_vel(id)
                  blocking = False

                  servo.move_degrees( ang, vel, blocking)

            else: # for direction-velocity controlled
                  vel = servodata.get_vel(id)
                  dir = servodata.get_dir(id)

                  servo.set_dir_vel(dir, vel)


        except KeyError:
            print "\ncaught key error.."
            raise



def get_feedback(servoIds):
    # input: list of servo ID's
    global servoDict
    dataObj = Pose()


    for id in servoIds:
#        print "per id: ", id

        if not id in servoDict:
               servoDict[id] = Robotis_Servo(dyn, id)

        dataObj.set_ang(id, math.degrees( servoDict[id].read_angle() ) )

    return dataObj