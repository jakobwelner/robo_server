'''
General todo:
      speedup Maya socket query:
            don't make new connections on each query
            run query on a different Process as quick as possible to always fill up queue.
                  assign time-stamp on data and add data to Queue
                  run servo slightly behind latest query to sample inputs and set speed for inbetweens
            

      servo query speedup
            query all servos simultaniously, join processes

      'run' should be Pipe not Queue

      query framenumber from Maya to verify timing
'''


from multiprocessing import Process, Queue
import time

import maya_handle; reload(maya_handle)
import servo_control; reload(servo_control)

### Importing to unpickle anim data
from dataset import *


def main_controller(run, cmd_queue, IGNORE_SERVOS):

      delayed = False
      if not IGNORE_SERVOS:
            servo_control.attach_usb()

      #### to use as feedback for controlling Maya, disable torque
      #servo_control.disable_torque(self.SERVOS)


      print "Starting Robo Server..."
      try:
            prev_stamp = 0.0
            prev_time = time.time()

            while run.qsize():
                  if cmd_queue.qsize():
                        t_stamp, cmd_data = cmd_queue.get()
                        curr_time = time.time()

                        stamp_diff = t_stamp - prev_stamp
                        time_diff = curr_time - prev_time

                        ### Allowing time to pass till difference between prev_time
                        ### and time.time() is same or larger than stamp_diff
                        delayed = False
                        if stamp_diff >= time_diff:
                              time.sleep(stamp_diff - time_diff)
                              delayed = True

                        if not IGNORE_SERVOS:
                              ### Send to servos
                              servo_control.broadcast_servos(cmd_data)


                        ### Printing info
                        fps_string = "FPS: " + str((1.0 / (time.time() - prev_time)))
                        if delayed:
                              fps_string += " delayed: " + str(stamp_diff - time_diff)
                        print fps_string

                        prev_stamp = t_stamp
                        prev_time = time.time()
                  else:
                      print "Cmd_Queue is empty..."

                        
            print "Server loop end.."

      except KeyboardInterrupt:
            print "start_main: exiting by keyboardinterrupt"

      finally:
            print "Closing Robo Server"
            print "robo_server.main_loop() >> cleaning up after quitting"

            if not IGNORE_SERVOS:
                  servo_control.release_usb()






def cmd_queue_producer(run, cmd_queue, SERVOS):

      BUFFER_SIZE = 10
      Controller = maya_handle.Maya_Controller(SERVOS)

      print "Starting controller query service"

      while run.qsize():
            if BUFFER_SIZE > cmd_queue.qsize():
                  # query controller
                  controllerData = Controller.get_pose()
                  t_stamp = time.time()
                  cmd_queue.put((t_stamp, controllerData))

      print "Exiting buffer_controller..."





class Robo_Server():
      def __init__(self, direct_control=1):

            self.direct_control = direct_control

            self.SERVOS = (2,3,4,)
            self.IGNORE_SERVOS = True

            self.cmd_queue = Queue()
            self.run = Queue()
            


      def start(self):
            if self.run.qsize() == 0:
                  self.run.put(1)
                  
            try:
                  if not self.cmd_queue.qsize() and not self.direct_control:
                        print "Robo_Server.run() >> cmd_queue is empty. Either run with direct control or load Anim first"
                  else:
                        if self.direct_control:
                              self.controller_process = Process(
                                    target = cmd_queue_producer,
                                    args = (self.run, self.cmd_queue, self.SERVOS))

                              self.controller_process.start()

                        self.server_process = Process(
                              target = main_controller,
                              args = (self.run, self.cmd_queue, self.IGNORE_SERVOS))

                        self.server_process.start()
                      

            except KeyboardInterrupt:
                  self.stop()

                  print "start_server: exiting by keyboardinterrupt"



      def stop(self):
            _ = self.run.get()
            self.server_process.terminate()

            if self.direct_control:
                  self.controller_process.terminate()



      def load(self, filepath):

            try: import cPickle as pickle
            except ImportError: import pickle as pickle

            pickled = 0
            try:
                  file = open(filepath, 'rb')
                  pickled = pickle.load(file)
            except IOError:
                  raise StandardError,'# maya_handle.load >> Could not open file. Permissions most probably denied. Fix it and try again'
            else:
                  file.close()

            #adding pickled data to queue
            for cmd in pickled.get_iter():
                  self.cmd_queue.put(cmd)



      def save_maya_anim(self, filepath):
            Controller = maya_handle.Maya_Controller(self.SERVOS)
            Controller.save_anim(filepath)
