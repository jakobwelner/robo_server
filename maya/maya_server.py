import SocketServer
from multiprocessing import Process, Queue
import time

#from threading import Thread

HOST = 'localhost'
PORT = 6001


class SingleTCPHandler(SocketServer.BaseRequestHandler):
    "One instance per connection.  Override handle(self) to customize action."
    def handle(self):
        # self.request is the client connection
        print "requesting data..."
        data = self.request.recv(1024)  # clip input at 1Kb
        
        ### Do stuff
        #reply = pipe_command(my_unix_command, data)
        print repr(data)
        
        reply = "Replying from Maya with data: " + str(data)
        
        if reply is not None:
            self.request.send(reply)
        self.request.close()


class SimpleServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
      # Ctrl-C will cleanly kill all spawned threads
      daemon_threads = True
      # much faster rebinding
      allow_reuse_address = True

      def __init__(self, server_address, RequestHandlerClass):
            SocketServer.TCPServer.__init__(self, server_address, RequestHandlerClass)





class server_process():
      def __init__(self):
            pass

      def run(self):
            self.queue = Queue()
            start_process()
#
#            try:
#                  self.queue = Queue()
#                  self.queue.put(1)
#
#                  print "starting process.."
#                  self.server_process = Process(target=self.start_process, args=(self.queue,))
#                  self.server_process.start()
#
#                  time.sleep(10)
#                  while self.queue.qsize():
#                        print "Queue: ", self.queue.get()
#
#            except KeyboardInterrupt:
#                  print "start_server: exiting by keyboardinterrupt"
#                  self.server_process.terminate()


      def shutdown(self):
            self.server_process.terminate()


      def start_process(queue):
            print "does anything come through?"
            queue.put("data from process")
            
            self.server = SimpleServer((HOST, PORT), SingleTCPHandler)
            # terminate with Ctrl-C
            try:
                  self.server.serve_forever()
            except KeyboardInterrupt:
                  self.server.shutdown()


