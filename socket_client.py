#Based on Example: http://kmkeen.com/socketserver/
import socket

def client(val, PORT=6001, HOST="localhost"):
      try:
            # SOCK_STREAM == a TCP socket
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            #sock.setblocking(0)  # optional non-blocking
            sock.settimeout(5)
            sock.connect((HOST, PORT))

            sock.send(val)
            reply = ""
            try:
                        size = sock.recv(6)
                        print "size: ", size
                        try:
                              reply = sock.recv(int(size))
                              print "Reply: ", reply
                        except ValueError:
                              pass
                        
#                        reply = sock.recv(1024)
#                        reply = recv_size(sock)
#                        reply = recv_basic(sock)
#                        reply = receive_reply(sock)
            except:
                  print "caught sock.recv exception"
                  raise
            
            finally:
                  sock.close()

            return reply

      except:
            print "caught exception while trying to connect to server. Is it running?"
            raise



#
#def recv_basic(the_socket):
#    total_data=[]
#    while True:
#        data = the_socket.recv(8192)
#        print data
#        if not data: break
#        total_data.append(data)
#    return ''.join(total_data)
#
#
#def recv_size(the_socket):
#    import sys
#    import struct
#
#    #data length is packed into 4 bytes
#    total_len = 0
#    total_data = []
#    size = sys.maxint
#    size_data = ''
#    sock_data = ''
#    recv_size = 1024#8192
#
#    while total_len<size:
#        print "before recv"
#        print "total_len: ", repr(total_len)
#        print "size: ", repr(size)
#        print "total_data: ", repr(total_data)
#
#        sock_data = the_socket.recv(recv_size)
#
#        if not total_data:
#            print "bla"
#            print "socket_data ", sock_data
#
#            if len(sock_data)>4:
#                print "blu"
#                size_data += sock_data
#
#                size_chunk = size_data[1:][:4]
#                print "Size chunk ", size_chunk
#                size = struct.unpack('>i', size_chunk)[0]
#                recv_size = size
#                if recv_size > 524288:
#                    recv_size = 524288
#                total_data.append(size_data[4:])
#
#            else:
#                print "bli"
#                size_data += sock_data
#        else:
#            print "blop"
#            total_data.append(sock_data)
#
#        print "still looping"
#        total_len = sum([len(i) for i in total_data ])
#    return ''.join(total_data)
#
#
#
#
#def receive_reply(sock):
#      start = sock.recv( 2 )
#      if start != '\xc2\xaa':
#            data = start + sock.recv(1024)
#            print "Start: ", repr(start)
#            print "\nNo header. Receiving blank: ", repr(data)
#
#      else:
#            print "Start: ", repr(start)
#            data_len = sock.recv( 2 )
#            print "len 2 char: ", repr(data_len)
#
#            data_len = ord(data_len[0]) + ord(data_len[1]) * 255
#            print "assembled length: ", data_len
#
#            data = sock.recv( data_len )
#            print "data: ", data
#
#      return data
#
#





