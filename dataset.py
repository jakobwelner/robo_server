class Pose(object):
      def __init__(self, type="angle"):
            self.data = {}
            
            if type == "direction-velocity" or type == "angle":
                  self.type = type
                  

      def set_ang(self, id, ang):
            self.__check_id(id)

            self.data[id]["ang"] = ang


      def set_angvel(self, id, angvel):
            self.__check_id(id)

            self.data[id]["angvel"] = angvel


      def set_vel(self, id, vel):
            self.__check_id(id)

            self.data[id]["vel"] = vel
            

      def set_dir(self, id, direction):
            self.__check_id(id)
            
            if direction == 1 or direction == -1:
                  self.data[id]["direction"] = direction
            else:
                  raise ValueError, "direction needs to be positive or negative 1: " + str(direction)


      def get_dir(self, id):
            return self.data[id]["direction"]


      def get_type(self):
            return self.type
      

      def get_angvel(self, id):
            return 100
            #return self.data[id]["angvel"]

      def get_vel(self, id):
            if self.data[id].has_key("vel"):
                  return self.data[id]["vel"]
            else:
                  return 400

      def get_ang(self, id):
            return self.data[id]["ang"]

      def get_ids(self):
            return self.data.keys()


      def __check_id(self, id):
            if not self.data.has_key(id):
                  self.data[id] = {}

      def get_dict(self):
            return self.data


class Anim():
      def __init__(self):
            self.anim_data = []

      def add_pose(self, pose_obj, t_stamp):
            self.anim_data.append((t_stamp, pose_obj))

      def get_iter(self):
            return self.anim_data

