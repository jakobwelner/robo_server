## Execute in Maya to allow connection
#import pymel.core as pm
#pm.commandPort( name=":6001", sourceType="mel", echoOutput=False )


from dataset import *
import socket_client; reload(socket_client)
import robotis.servo_config as sc
import os

try:
      import cPickle as pickle
except ImportError:
      import pickle as pickle







def import_maya_internal():

      maya_internal_module_name = "maya_internal"
      
      script_path = os.path.dirname( os.path.realpath(__file__) )
      path_split = script_path.split("\\")
      script_path = "/".join(path_split)
      
      add_path_cmd = 'python("sys.path.append(\\"%s\\")")' % script_path
      importStr = 'python("import %s")' % maya_internal_module_name

      socket_client.client('python("import sys")')
      socket_client.client(add_path_cmd)
      socket_client.client(importStr)

      return maya_internal_module_name








def mel_to_python(socket_package):
      # converting string received over mayas commandPort into python objs
      dump = socket_package.split("\n")[0] # removes trailing "\n\x00"
      listVals = dump.split("\t")

      r = []
      for v in listVals:
            try:
                  v = float(v)
            except:
                  pass
            r.append( v )

      return r
#
#
#def list_to_mel(list):
#      # converting python lists into mel list strings
#      melList = "{"
#      for e in list:
#            melList += '"' + str(e) + '",'
#
#      melList = melList[:-1]
#      melList += "}"
#
#      return melList



class Maya_Handle(object):
      def __init__(self, servoIDs):

            self.id_settings = {}
            try:
                  for id in servoIDs:
                        if sc.servo_param.has_key( id ):
                              self.id_settings[id] = sc.servo_param[ id ]
                        else:
                              raise ValueError, 'Error: servo_id '+ str(id) + ' not found in servo_config.py.'
            except:
                  print 'Warning: servo_config.py not found.  failing...\n'
                  raise
            
            
      def bind_to_existing(self, namespace):
            # namespace being either controller or feedback
            ctrl_chain = []
            for id in self.id_settings:
                  ctrl_chain.append( self.id_settings[id][namespace] )
                  
            return ctrl_chain
            







class Maya_Controller(Maya_Handle):
      def __init__(self, servoIDs, setup=False):

            Maya_Handle.__init__(self, servoIDs)
            self.MAYA_QUERY_PROC = "query_attrs"
            self.namespace = "controller"

            if setup:
                  # build controller chain
                  self.ctrl_list = self.setupJointChain(self.namespace, (0, 0, 0))
            else:
                  #hooks onto existing jointchains in Maya, if possible
                  self.ctrl_list = self.bind_to_existing(self.namespace)

            ### importing Maya internal python-tools
            self.maya_internal = import_maya_internal()




      def get_pose(self):

            pCmd = self.maya_internal + '.get_pose_pickle(%s, %s)' % (repr(self.id_settings.keys()), repr(self.ctrl_list))
            socket_vals = socket_client.client("python(\"" + pCmd + "\")")

            data_obj = pickle.loads(socket_vals)

            return data_obj





      def save_anim(self, filepath):
            
            data_obj = self.get_anim()
            
            ### Trying to open file
            try:
                  file = open(filepath, 'wb')

            ### Input/Output error handler
            except IOError:
                  print ""
                  print '# maya_handle.save_anim >> Could not open file. Most likely permissions or wrong dir. Fix it and try again',

            else:
                  try:
                        pickle.dump(data_obj, file, -1)

                  except:
                        raise StandardError, '# maya_handle.save_anim >> Could not write to file'
                  else:
                        print ""
                        print '# maya_handle.save_anim >> File was successfully written at: ' + filepath,

                  file.close()






      def get_anim(self):
            pCmd = self.maya_internal + '.get_anim_pickle(%s, %s)' % (repr(self.id_settings.keys()), repr(self.ctrl_list))
            socket_vals = socket_client.client("python(\"" + pCmd + "\")")

            data_obj = pickle.loads(socket_vals)

            return data_obj




class Maya_Feedback(Maya_Handle):
      def __init__(self, servoIDs, setup=False):

            Maya_Handle.__init__(self, servoIDs)
            self.MAYA_SET_PROC = "set_attrs"
            self.namespace = "feedback"

            if setup:
                  # build reference chain for servo feedback
                  self.ctrl_list = self.setupJointChain(self.namespace, (10, 0, 0))
            else:
                  # hooks onto existing jointchains in Maya, if possible
                  self.ctrl_list = self.bind_to_existing(self.namespace)

            self.setup_maya_setProc()



      def setup_maya_setProc(self):
            setProc_cmd = '''\
                  global proc %s(string $attrs[], string $values[]) {\
                        for ($i = 0; $i < size($attrs); $i++){\
                              float $fCast = $values[$i];\
                              setAttr $attrs[$i] $fCast;}\
                  }''' % self.MAYA_SET_PROC

            socket_client.client( setProc_cmd )




      def write_feedback(self, pose_obj):
            # data as {<id> : value, <id>: value}
            attrName = []
            attrVal = []

            for i in pose_obj.get_ids():
                  attrName.append(self.id_settings[i][self.namespace])
                  attrVal.append(pose_obj.get_ang(i))

            socket_client.client(self.MAYA_SET_PROC + "(%s, %s)" % (list_to_mel(attrName), list_to_mel(attrVal)))


	
	
	
	
	
	